<?php

class PlaceMarkersController extends AppController {
	
	//public $name = 'PlaceMarkers'; //not strictly needed
	public $helpers = array('Qrcode');
	
	public function index() {
		//be sure to declare publiciy of function
		// we use the buildings find method
		$this->set('allPlaceMarkers',$this->PlaceMarker->find('all')); //sends data to our view first var holds data from DB
	}
	
	function view($id = NULL) {
		//$this->Building->read(NULL, $name); //takes 2 params, first is list of fields we want to return, 
		$this->set('placeMarker',$this->PlaceMarker->read(NULL, $id));
		/*$this->set('building',$this->Building->find('list', array(
			'conditions' => array( 'id =' => $id)
				)
			)
		);*/
	}
	
	function add(){
		// this saves a building
		if($this->request->is('post')) {
			if($this->PlaceMarker->save($this->request->data)){
				$this->Session->setFlash('The Place Marker was successfully saved');
				//TODO redir to a QR code
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Place Marker was not saved');
			}
			
		}
	}
	function test(){
		$placeMarker = $this->PlaceMarker->find('first', array(
				'fields'     => 'PlaceMarker.id',
				'conditions' => array(
					'room_id'          => '1',
					'room_building_id' => '1'),
				'group'      => array('PlaceMarker.id DESC')));
			
		$this->set('test', $placeMarker);
	}
}
