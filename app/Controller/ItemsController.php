<?php

class ItemsController extends AppController {
	
	//public $name = 'Item'; //not strictly needed
	var $uses = array('Item', 'Room', 'Building');
	public $helpers = array('Qrcode');
	
	public function index() {
		//be sure to declare publiciy of function
		// we use the Items find method
		$this->set('allItems',$this->Item->find('all')); //sends data to our view first var holds data from DB
		
		//not working 'sets'
		$this->set('rooms', $this->Room->find('list', array(
			'fields' => array('Room.number'))));
		$this->set('buildings', $this->Building->find('list', array(
			'fields' => array('Building.name'))
		));
		
	}
	
	function view($id = NULL) {
		//$this->Item->read(NULL, $name); //takes 2 params, first is list of fields we want to return, 
		$this->set('Item',$this->Item->read(NULL, $id));
	}
	
	function add(){		
		//Need these for forms
		$this->set('buildings', $this->Building->find('list', array(
			'fields' => 'Building.name')));
		$this->set('rooms', $this->Room->find('list', array(
			'fields' => 'Room.number')));
		
		
		// this saves a Item
		/*
		if($this->request->is('post')) {
			$buildingId = $this->request->data['Item']['building_id'];
			$roomId = $this->request->data['Item']['room_id'];
		*/
			
		// this saves an Item		
		if($this->request->is('post')) {
			if($this->Item->save($this->request->data)){
				$this->Session->setFlash('The Item was successfully saved');
				//TODO redir to a QR code
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Item was not saved');
			}
			
		}

	}
	
	function remove($id = NULL){
		// check User input
		if($id == NULL){
			$this->Session->setFlash('No such item!');
			$this->redirect(array('action'=>'index'));	
			return;
		}
		
		// Delete
		$this->Item->delete($id);
		
	}
	
	function scan($id = NULL){
		// check User input
		if($id == NULL){
			$this->Session->setFlash('No such item!');
			$this->redirect(array('action'=>'index'));	
			return;
		}
		
		// What happens when we scan an Item
		
		
		// Get the current information about the item
		$currItem = $this->Item->findById($id);
		
		// Update last inspection date and next inspection_date
		$currDate           = date('Y-m-d h:i:s');
		$dateNextInspection = date('Y-m-d', strtotime('+'.$currItem['Item']['inspect_interval'].' days'));
		
		$this->Item->id = $id;
		$this->Item->saveField('inspect_date', $currDate);
		$this->Item->saveField('next_inspection_date', $dateNextInspection);
		
		// Give our view the information about the item and redir
		$this->Session->setFlash('Thank You for Inspecting this item. ');
		$this->redirect(array(
			'action'=>'view',
			$id));	
	}
	
	function QR($id = NULL){
		// Displays a QR code for the selected building
		$this->set('QR', 'Http://'.$_SERVER['SERVER_NAME'].$this->webroot.'items/scan/'.$id);
	}
	
	function missing($id = NULL){
	// check User input
		if($id == NULL){
			$this->Session->setFlash('No such item!');
			$this->redirect(array('action'=>'index'));	
			return;
		}
		
		$this->Item->id = $id;
		$this->Item->saveField('is_missing', '1');
		
		// Give our view the information about the item and redir
		$this->Session->setFlash('Thank You for reporting this item missing');
		$this->redirect(array(
			'action'=>'view',
			$id));	
	}
	
	function returned($id = NULL){
	// check User input
		if($id == NULL){
			$this->Session->setFlash('No such item!');
			$this->redirect(array('action'=>'index'));	
			return;
		}
		
		// set is_missing to false
		$this->Item->id = $id;
		$this->Item->saveField('is_missing', '0');
		
		// Give our view the information about the item and redir
		$this->Session->setFlash('Thank You for returning this item!');
		$this->redirect(array(
			'action'=>'view',
			$id));	
	}
	
	function move($id = NULL){

		
		// Find information about the item and set it in the view
		$currItem =  $this->Item->findAllById($id);
		$this->set('item', $currItem);
		
		// Find all the rooms in the building
		$this->set('rooms', $this->Room->find('list', array(
			'fields'     => array('Room.id','Room.number'),
			'conditions' => array(
				'Room.building_id' => $currItem[0]['Item']['building_id']))));

		// 	this saves an Item		
		if($this->request->is('post')) {
			// Get the request data and set which Item we want to update
			$requestData = $this->request->data;
			$this->Item->id = $currItemp[0]['Item']['id'];
			
			if($this->Item->saveField('rooms_id', $requestData['Item']['rooms_id'])){
				$this->Session->setFlash('The Item was successfully saved');
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Item was not saved');
			}
			
		}
	}
	
}
