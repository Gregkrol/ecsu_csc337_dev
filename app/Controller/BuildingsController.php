<?php

class BuildingsController extends AppController {
	
	//public $name = 'building'; //not strictly needed
	public $helpers = array('Qrcode');
	var $uses = array('Item', 'Room', 'Building');
	
	public function index() {
		//be sure to declare publiciy of function
		// we use the buildings find method
		$this->set('allBuildings',$this->Building->find('all')); //sends data to our view first var holds data from DB
	}
	
	function view($id = NULL) {
		$this->set('building',$this->Building->read(NULL, $id));
		$this->set('rooms', $this->Room->find('all', array(
			'conditions' => array (
				'Room.building_id' => $id))));
	}
	
	function add(){
		// this saves a building
		if($this->request->is('post')) {
			if($this->Building->save($this->request->data)){
				$this->Session->setFlash('Building Saved');
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Post was not saved');
			}
			
		}
	}
	
	function remove(){
		// If they need to remove a building
	}
	
	function scan($id = NULL){
		// QR scan
		// What happens when a person scans the QR code..
		
		// Get the Building info (name)
		$this->set('building',$this->Building->find('all', array(
			'conditions' => array('Building.id' => $id),
			'fields'     => array('Building.id', 'Building.name'),
			'recursive'  => 0)));

		// Get a list of items that need to be scanned
		$this->set('items', $this->Item->find('all', array(
			'conditions' => array(
				'Item.next_inspection_date - CURDATE() <=' => '0',
				'Item.building_id'         => $id
				))));
	
	}
	
	function QR($id = NULL){
		// Displays a QR code for the selected building
		$this->set('QR', 'Http://'.$_SERVER['SERVER_NAME'].$this->webroot.'buildings/scan/'.$id);
	}
	
	function viewItems($id = NULL){
		$this->set('items', $this->Item->findAllByBuildingId($id));
	}
}
