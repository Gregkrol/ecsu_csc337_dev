<?php

class LocationIdsController extends AppController {
	
	public $name = 'LocationIds'; //not strictly needed
	public $helpers = array('Qrcode');
	
	public function index() {
		//be sure to declare publiciy of function
		// we use the buildings find method
		$this->set('allLocationIds',$this->LocationId->find('all')); //sends data to our view first var holds data from DB
	}
	
	function view($id = NULL) {
		//$this->Building->read(NULL, $name); //takes 2 params, first is list of fields we want to return, 
		$this->set('locationId',$this->LocationId->read(NULL, $id));
		/*$this->set('building',$this->Building->find('list', array(
			'conditions' => array( 'id =' => $id)
				)
			)
		);*/
	}
	
	function add(){
		// this saves a building
		if($this->request->is('post')) {
			if($this->LocationId->save($this->request->data)){
				$this->Session->setFlash('The Location ID was successfully saved');
				//TODO redir to a QR code
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Post was not saved');
			}
			
		}
	}
	function hello_world(){
		
	}
}
