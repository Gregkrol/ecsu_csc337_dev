<?php

class PathsController extends AppController {
	
	//public $name = 'building'; //not strictly needed
	public $helpers = array('Qrcode');
	
	public function index() {
		//be sure to declare publiciy of function
		// we use the buildings find method
		$this->set('allPaths',$this->Path->find('list', array(
			'fields' => array('Path.id', 'Path.name'),
			'group'  => array('Path.name'))
		)); //sends data to our view first var holds data from DB
	}
	
	function view($id = NULL) {
		/*
		 * View Handles most of our Logic
		 */
		
		if($this->Session->check('Path.pathName')){
			// If we have a pathName, we must be following a path
			$pathName = $this->Session->read('Path.PathName');
			
			if($this->Session->check('Path.status')){
				// Types of statuses: 'addStop', 'scanned'
				$pathStatus = $this->Session->read('Path.status');
				
				if($this->Session->check('Path.idType')){
					// Because we will always need the idType, we should read it and check for mistakes
					$idType = $this->Session->read('Path.idType');

					//Save
					// note, idType should be the field name
					if($this->Path->save(array('name' => $pathName, $idType => $id))){
						$this->Session->setFlash('Stop added');
						$this->Session->write('Path.pathName', $pathName);
						$this->redirect(array(
							'controller' => 'Path',
							'action'     => 'view'));
					}
				} else {
					// We cannot continue without an idType, so we return to index
					$this->Session->setFlash('Unknown Id type');
					$this->redirect(array('action' => 'index'));
				}
				if($pathStatus == 'addStop'){
					// add stop logic here
					
					
				} 
				elseif ($pathStatus == 'scanned'){
					
				}
				
				
				//No id, but status and pathname
			} else {
			//No status but pathname
				$this->set('path',$this->Path->find('list', array(
					'conditions' => array( 'Path.name' => $pathName))));
			}
		}
		else{
			
			if($id != null){
				// We need to return all stops that have the Path.Name that our View id has
				
				$pnArr = $this->Path->find('first', array(
							'fields'     => array('Path.name'),
							'conditions' => array('Path.id' => $id)));
				$PN = $pnArr['Path']['name'];
	
				//$this->set('path',$this->Path->read(NULL, $id));
				
				$this->set('path', $this->Path->find('all', array(
					'conditions' => array('Path.name' => $PN ),
					'recursive'  => 1)));
			}
			else{
				$this->Session->setFlash('Please select a path');
				$this->redirect(array('action'=>'index'));	
			}
			
		}
		
	}
	
	function beginPath($id = null){
		
		if($id != null){
			// if we have an id, set session data
			$path = $this->Path->find('first', array(
							'conditions' => array('Path.id' => $id)));
			$this->Session->write('Path.pathName', $path['Path']['name']);
			$this->redirect(array('action' => 'view', $id));
		} else {
			// if id == null, return to index, flash failure
			$this->Session->setFlash('No path selected!');
			$this->redirect(array('action' => 'index'));
		}
		
		
		
	}
	
	function viewStop() {
		//This is like our view, but we
		
	}
	
	function add(){
		// this saves a building
		if($this->request->is('post')) {
			if($this->Path->save($this->request->data)){
				$this->Session->setFlash('The Path was successfully saved');
				//TODO redir to a QR code
				$this->redirect(array('action'=>'view'));	
			} else {
				$this->Session->setFlash('The Path was not saved');
			}
			
		}
	}
	
	function addStop($pathName = null){
		// Things to set: pathName, status
		$this->Session->write('Path.pathName', '$pathName');
		$this->Session->write('Path.status', 'addStop');
		$this->redirect(array('action'=>'view'));
	}
	
	function remove(){
		// If they need to remove a building
	}
}
