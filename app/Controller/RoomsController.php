<?php

class RoomsController extends AppController {
	
	public $name = 'Rooms'; //not strictly needed
	var $uses = array('Room', 'Building', 'Item');
	public $helpers = array('Qrcode');
	
	public function index() {
		// Gives our index.ctp an array of arrays that is every room in our Room table
		$this->set('allRooms',$this->Room->find('all')); //sends data to our view first var holds data from DB
		
	}
	
	function view($id = NULL) {
		// Gives our view.ctp an array that is the data of the row in the Room table of the room with $id
		$this->set('Room',$this->Room->read(NULL, $id));
		
	}
	
	function add(){
		// list buildings (for our forms)
		$this->set('buildings', $this->Building->find('list', array(
			'fields' => array('Building.name'))
		));
		
		// this saves a building		
		if($this->request->is('post')) {
			if($this->Room->save($this->request->data)){
				$this->Session->setFlash('The Room was successfully saved');
				//TODO redir to a QR code
				$this->redirect(array('action'=>'index'));	
			} else {
				$this->Session->setFlash('The Room was not saved');
			}
			
		}
	}
	
	function remove(){
		
	}
	
	function scan($id = NULL){
		// redirect to our view (since all functionality is in our view)
		$this->redirect(array(
			'action' => 'view',
			$id));
		
	}
	
	function QR($id = NULL){
		// Displays a QR code for the selected building
		$this->set('QR', 'Http://'.$_SERVER['SERVER_NAME'].$this->webroot.'rooms/scan/'.$id);
	}
	
	function test(){
		$this->set('test', $this->Room->find('all', array(
			'fields'  => array('Room.id','Room.number','Room.building_id'),
			'group'   => 'Room.building_id',
			'recurse' => 0)));
	}
	
}
