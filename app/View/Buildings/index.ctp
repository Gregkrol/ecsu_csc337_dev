<!--<div id = "Buidlings"> -->

<h1> All Buildings<small>in Database</small> </h1>
	

<table class="table table-striped table-bordered ">
  <tr>
    <th>Building Name</th>
    <th></th>
  </tr>
  <?php foreach($allBuildings as $building): ?>
  <tr>
    <td>
    	<?php 
	    echo $this->Html->link(
	        $building['Building']['name'],
	        array(
	          'controller' => 'buildings',
	          'action'     => 'view',
	          $building['Building']['id']
	        )
	    ); 
	    ?>
    </td>
    <td>
    	<?php 
	    echo $this->Html->link(
	        'View expired items',
	        array(
	          'controller' => 'buildings',
	          'action'     => 'scan',
	          $building['Building']['id']
	        ), array(
	        	'class' => 'btn btn-info')
	    ); 
	    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>

<!-- </div> -->
<?php 
echo $this->Html->link(
       'Add a Building', array(
          'controller' => 'buildings',
          'action'     => 'add'),array(
		  'class'      => 'btn')
	
    );
?>
