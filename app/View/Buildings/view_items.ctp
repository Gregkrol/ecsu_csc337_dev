<h2><?php echo $items[0]['Building']['name']; ?></h2>

<h3>Items</h3>
<table class="table table-striped table-bordered ">
<thead>
  <tr>
  	<th>Type</th>
    <th>Serial Number</th>
    <th>Last Inspection Date</th>
    <th>Report Missing</th>
  </tr>
</thead>

<tbody>
  <?php foreach ($items as $item): ?>
  <tr>
  	<td>
  		<?php echo $item['Item']['type'];?>
  	</td>
    <td>
    <?php 
    echo $this->Html->link(
        $item['Item']['serial_number'],
        array(
          'controller' => 'Items',
          'action'     => 'view',
          $item['Item']['id']
        )
    ); 
    ?>
    </td>
    <td>
    	<?php echo $item['Item']['inspect_date'];?>
    </td>
    <td>
    	<?php 
		echo $this->Html->link(
			        'Missing', array(
			        'controller' => 'items',
			        'action'     => 'missing',
			        $item['Item']['id']
			        ),array(
			 		 'class'      => 'btn btn-danger')
		); 
		?>
    </td>
  </tr>	
  <?php endforeach; ?>
</tbody>
</table>

<p>
	<?php 
	echo $this->Html->link(
		        'Add a room', array(
		        'controller' => 'rooms',
		        'action'     => 'add',
		        $items[0]['Building']['id']
		        )
	); 
	?>
	
</p>