<?php $i = 0;
?>

<h1>Items to scan
	<small><?php 
		echo $this->Html->link(
		        $building[0]['Building']['name'],
		        array(
		          'controller' => 'buildings',
		          'action'     => 'view',
		          $building[0]['Building']['id']
		        )
		    );  ?>
	</small> 
</h1>
	
<table class="table table-striped table-bordered ">
	<thead>
		<tr>
			<th></th>
			<th>Room</th>
			<th>Type</th>
			<th>Serial Number</th>
			<th>Last Inspected</th>
			<th>Report Missing</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($items as $item): ?>
		<tr>
			<td>
				<?php echo $i++;?>
			</td>
			<td>
				<?php 
				echo $this->Html->link(
					$item['Room']['number'],
						array(
							'controller' => 'rooms',
							'action'     => 'view',
							$item['Room']['id']
						)
					); 
				?>
			</td>
			<td>
				<?php echo $item['Item']['type'];?>
			</td>
			<td>
				<?php 
				echo $this->Html->link(
					$item['Item']['serial_number'],
						array(
							'controller' => 'items',
							'action'     => 'view',
							$item['Item']['id']
						)
					); 
				?>
			</td>
			<td>
				<?php echo $item['Item']['inspect_date'];?>
			</td>
			<td>
			    <?php 
				echo $this->Html->link(
			        'Missing', array(
				        'controller' => 'items',
				        'action'     => 'missing',
				        $item['Item']['id']
				        ),array(
				 		 	'class'  => 'btn btn-danger')
				); 
				?>
		    </td>
		</tr>	
		<?php endforeach; ?>
	</tbody>
</table>

<!-- </div> -->

