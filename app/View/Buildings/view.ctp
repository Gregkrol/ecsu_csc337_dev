<div class = "well">
<h2><?php echo $building['Building']['name']; ?></h2>
</div>

<?php 
	// TODO: we can parse through the rooms of the building and give information
	//       about whether or not we should goto that room and scan items
	//print_r($building);
	//print_r($rooms);?>

<ul class="nav nav-tabs nav-stacked">
<li>
	<?php 
	    echo $this->Html->link(
	        'View expired items',
	        array(
	          'controller' => 'buildings',
	          'action'     => 'scan',
	          $building['Building']['id']
	        )); 
    ?>
</li>
<li>
    <?php 
	     echo $this->Html->link(
	        'View All items',
	        array(
	          'controller' => 'buildings',
	          'action'     => 'viewItems',
	          $building['Building']['id']
	        )); 
	?>
</li>
<li>
	<?php 
		echo $this->Html->link(
			        'Add a room', array(
			        'controller' => 'rooms',
			        'action'     => 'add',
			        $building['Building']['id']
			        )
		); 
	?>
</li>
<li>
	<?php 
	    echo $this->Html->link(
	        '<i class = "icon-qrcode"></i> Print QR code',
	        array(
	          'controller' => 'buildings',
	          'action'     => 'QR',
	          $building['Building']['id']
	        ), array (
				'escape' => false,
        	)); 
    ?>
</li>
</ul>



<p>
	<?php 
	if(!array_key_exists('Room', $building)){
		echo "No rooms were found.";
	}
	?>	
</p>

<h3>Rooms</h3>
<table class="table table-striped table-bordered ">
<thead>
  <tr>
  	<th></th>
    <th>Room Number</th>
    <td>Items</td>
    <th>QR code</th>
  </tr>
</thead>
<tbody>
  <?php 
  $i = 1;
  foreach ($building['Room'] as $room): ?>
  <tr>
  	<td>
  		<?php echo $i++?>
  	</td>
    <td>
    <?php 
    echo $this->Html->link(
        $room['number'],
        array(
          'controller' => 'rooms',
          'action'     => 'view',
          $room['id']
        )
    ); 
    ?>
    </td>
    <td>
    <?php 
	    echo $this->Html->link(
	        '<i class = "icon-search"></i>',
	        array(
	          'controller' => 'rooms',
	          'action'     => 'view',
	          $room['id']
	        ), array (
				'escape' => false,
        	)); 
    ?> 
    </td>
    <td>
    <?php 
    echo $this->Html->link(
        '<i class = "icon-qrcode"></i> Print QR code',
        array(
          'controller' => 'rooms',
          'action'     => 'QR',
          $room['id']
        ),array (
			'escape' => false,
        	)
    ); 
    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</tbody>
</table>

