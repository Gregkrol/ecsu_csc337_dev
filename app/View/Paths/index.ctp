<!--<div id = "Paths"> -->
<h1> All Paths<small>available</small> </h1>

<table class="table table-striped table-bordered ">
  <tr>
    <th>Path Name</th>
    <th>Path Description</th>
  </tr>
  <?php foreach($allPaths as $pathId => $pathName): ?>
  <tr>
    <td>
    <?php 
    echo $this->Html->link(
        $pathName,
        array(
          'controller' => 'paths',
          'action'     => 'view',
          $pathId
        )
    ); 
    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>

<!-- </div> -->
<?php 
echo $this->Html->link(
       'Add a Path', array(
          'controller' => 'paths',
          'action'     => 'add'),array(
		  'class'      => 'btn')
	
    );
?>
