<?php 
/*
 * 
 * 
 */
print_r($path);
$i = 0;
?>

<h1><?php 
	if(isset($path[0]['Path']['name'])){
		$pathName = $path[0]['Path']['name'];
		$pathId   = $path[0]['Path']['id'];
		echo $path[0]['Path']['name'];
	} else {
		echo $path['Path']['name'];
		$pathName = $path['Path']['name'];
		$pathId   = $path['Path']['id'];
	}
	
	?> <small><?php ?></small></h1>


<h3>Stops:</h3>
<table class="table table-striped table-bordered ">
  <tr>
  	<th></th>
    <th>Type</th>
    <th>Detail</th>
    <th>Last visit</th>
  </tr>
  
  <?php foreach ($path as $stop): ?>
  <tr>
  	<td>
  	<?php echo $i++;?>
  	</td>
  	
    <td>
    <?php 
    
    if($stop['Path']['buildings_id'] != null){
    	echo "Building";
    }
    elseif($stop['Path']['rooms_id'] != null){
    	echo "Room";
    }
    elseif($stop['Path']['place_markers_id'] != null){
    	echo "PlaceMarker";
    }
    elseif($stop['Path']['items_id'] != null){
    	echo "Item";
    }
    else {
    	echo "Path Created";
    }
    ?>
    </td>
    
    <td>
    <?php 
    if($stop['Path']['buildings_id'] != null){
    	// Link to Building
    	echo $this->Html->link(
	        $stop['Building']['full_name'],
	        array(
	          'controller' => 'buildings',
	          'action'     => 'view',
	          $stop['Building']['id']
	        )
	    ); 
    }
    elseif($stop['Path']['rooms_id'] != null){
    	// Link to room
        echo $this->Html->link(
	        $stop['Room']['number'],
	        array(
	          'controller' => 'rooms',
	          'action'     => 'view',
	          $stop['Room']['id']
	        )
	    ); 
    }
    elseif($stop['Path']['place_markers_id'] != null){
    	// Link to PlaceMarker
    	echo $this->Html->link(
	        'Place Marker',
	        array(
	          'controller' => 'placeMarkers',
	          'action'     => 'view',
	          $stop['PlaceMarker']['id']
	        )
	    ); 
    }
    elseif($stop['Path']['items_id'] != null){
    	echo $this->Html->link(
	        $stop['Item']['serial_num'],
	        array(
	          'controller' => 'items',
	          'action'     => 'view',
	          $stop['Item']['id']
	        )
	    ); 
    }
    else {
    	echo "";
    }
    ?>
    </td>
    
    <td>
    <?php echo $stop['Path']['last_visit'];?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>


<p>
<?php 
echo $this->Html->link(
       'Add a Stop', array(
          'controller' => 'paths',
          'action'     => 'addStop',
		  $pathName),
		  array('class' => 'btn')
	
    );
?>
<?php 
echo $this->Html->link(
       'Follow This Path', array(
          'controller' => 'paths',
          'action'     => 'beginPath',
		  $pathId),
		  array('class' => 'btn')
	
    );
?>

</p>