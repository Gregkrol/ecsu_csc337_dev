<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       Cake.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
$QRappDescription = __d('cake_dev', 'ECSU QR');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php //echo $cakeDescription
			  echo $QRappDescription; ?>:
		<?php echo $title_for_layout; ?>
	</title>
	
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic'); //cakePHP css, no longer needed
		echo $this->Html->css('bootstrap');
		echo $this->Html->css('bootstrap-responsive');
		echo $this->Html->css('custom');
		
		echo $this->Html->script('jquery');
		echo $this->Html->script('bootstrap');
		echo $this->Html->script('bootstrap-collapse');
		echo $this->Html->script('scan');
		

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>

<body>
<?php 
// determine which scan to call when the scan button is hit
// Either Quickmark or android
$ipad = (bool) strpos(env('HTTP_USER_AGENT'), 'iPad');
if($ipad === True){
	$scanType = 'ios_scan()';
} else {
	$scanType = 'droid_scan()';
}

?>
    <div class="navbar navbar-fixed-bottom">
      <div class="navbar-inner">
        <div class="container">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo 'HTTP://'.$_SERVER['SERVER_NAME'].$this->webroot; ?>">QRIM</a>
          <div class="nav-collapse collapse">
            <ul class="nav">
              <li class = "active">
              <?php 
              echo $this->Html->link('Scan','javascript:void(0)',array(
              	'onclick' => $scanType));
              ?></li>
              <li>
              <?php
              	echo $this->Html->link('Buildings',array(
              		'controller' => 'buildings',
              		'action'     => 'index'));
              ?>
              </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
  <div class="container" id="content">
    <div class="row-fluid">
      <div class="span12">
			<?php #echo $this->Session->flash(); ?>
			<?php echo $this->TwitterBootstrap->flash( ); ?>
			<?php echo $this->fetch('content'); ?>
      </div>
    </div>
  </div>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
