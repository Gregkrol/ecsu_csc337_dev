<h2>
	Item with Serial number: <?php echo $Item['Item']['serial_num']; ?> 
	<small><?php 
		echo $this->Html->link(
		        $Item['Building']['full_name'], array(
		        'controller' => 'buildings',
		        'action'     => 'view',
		        $Item['Building']['id']
		        )
		); ?> : Room <?php 
		echo $this->Html->link(
		        $Item['Room']['number'], array(
		        'controller' => 'buildings',
		        'action'     => 'view',
		        $Item['Room']['id']
		        )
		);
		; ?>
	</small>
</h2>

<h3>Information:</h3>
<table class="table table-striped table-bordered table-condensed">
  <tr>
    <th>Serial Number</th>
  </tr>

  <tr>
    <td>
    <?php 
    echo $this->Html->link(
        $Item['Item']['serial_num'],
        array(
          'controller' => 'Items',
          'action'     => 'view',
          $Item['Item']['id']
        )
    ); 
    ?>
    </td>
  </tr>	

</table>


<p>
	<?php 
	echo $this->Html->link(
		        'Add an item to this room', array(
		        'controller' => 'items',
		        'action'     => 'add',
		        $Item['Room']['id']
		        )
	); 
	?>
</p>