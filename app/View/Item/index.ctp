<!--Items--->
<div id = "allItems">
	<h2> All Items </h2>

	<table class="table table-striped table-bordered table-condensed">
		<tr>
			<th>Serial Number</th>
			<th>Building</th>
			<th>Room</th>
		</tr>
		
		<?php
		 /*
		  * Here, we are given an object called $allItems from our Controller. 
		  * We have access to that object. 
		  */
		  
		foreach($allItems as $Item): ?>
		 
		<tr>
			<td><?php // Item Link
				echo $this->Html->link(
					$Item['Item']['serial_num'], array(
			          'controller' => 'items',
			          'action'     => 'view',
			          $Item['Item']['id'] )
			    ); ?>
			</td>
			<td><?php // Building Link
				echo $this->Html->link(
					$Item['Building']['full_name'], array(
			          'controller' => 'buildings',
			          'action'     => 'view',
			          $Item['Building']['id'] )
			    );?>
			</td>
			<td><?php // Room Link
				echo $this->Html->link(
					$Item['Room']['number'], array(
			          'controller' => 'rooms',
			          'action'     => 'view',
			          $Item['Room']['id'] )
			    );?>
		    </td>
		</tr>
		<?php endforeach; ?>
		
	</table>
	
</div>