<?php

 
if (Configure::read('debug') == 0):
	throw new NotFoundException();
endif;

App::uses('Debugger', 'Utility');
?>

<h2> QR code application written with Cakephp </h2>

<div class="hero-unit">
	<h1>Welcome to the QR management system!</h1>
</div>


<ul class = "nav nav-tabs nav-stacked">
	<li>
		<?php
		echo $this->Html->link(
			'Select a Building',
			array('controller' => 'buildings'),
			array()
		); ?>
	</li>
	<li>
		<?php
		echo $this->Html->link(
			'View all Rooms',
			array('controller' => 'rooms'),
			array()
		); ?>
	</li>
	<li>
		<?php
		echo $this->Html->link(
			'View all Items',
			array('controller' => 'items'),
			array()
		); ?>
	</li>
</ul>


