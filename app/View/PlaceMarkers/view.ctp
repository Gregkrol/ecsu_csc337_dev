<h1>Place Marker <?php echo $PlaceMarker['Room']['number']; ?> 
	<small><?php 
		echo $this->Html->link(
	        $PlaceMarker['Building']['full_name'],
	        array(
	          'controller' => 'Buildings',
	          'action'     => 'view',
	          $PlaceMarker['Building']['id']
	        )
	    ); ?>
	</small></h1>

<?php //print_r($Room); // for debugging: remove quotes to see what we are trying to parse?>
<p>
</p>

<h3>Item</h3>
<table class="table table-striped table-bordered ">
  <tr>
    <th>Serial Number</th>
  </tr>
  <?php foreach ($PlaceMarker['Item'] as $item): ?>
  <tr>
    <td>
    <?php 
    echo $this->Html->link(
        $item['serial_num'],
        array(
          'controller' => 'Items',
          'action'     => 'view',
          $item['id']
        )
    ); 
    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>

<div class="well">
<?php echo $this->Qrcode->text('http://gregkrol.kodingen.com/cake/buildings/view/'.$PlaceMarker['Building']['id']); ?>
</div>

<p>
	<?php 
	echo $this->Html->link(
		        'Add an item', array(
		        'controller' => 'items',
		        'action'     => 'add',
		        $Room['Room']['id']
		        )
	); 
	?>
</p>

<?php echo $this->Qrcode->text('http://gregkrol.kodingen.com/cake/buildings/view/'.$building['Building']['id']); ?>