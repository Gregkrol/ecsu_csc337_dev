<!--<div id = "Place Markers"> -->
<h1> All Place Markers<small>set page description here</small> </h1>

<table class="table table-striped table-bordered">
  <tr>
  	<th>Number</th>
    <th>Building Name</th>
    <th>Room Number</th>
    <th>Item</th>
  </tr>
  <?php foreach($allPlaceMarkers as $placeMarker): ?>
  
  <tr>
    <td>
    <?php 
    echo $this->Html->link(
        $placeMarker['PlaceMarker']['id'],
        array(
          'controller' => 'placeMarkers',
          'action'     => 'view',
          $placeMarker['PlaceMarker']['id']
        )
    ); 
    ?>
    </td>
    <td>
    <?php 
    echo $this->Html->link(
        $placeMarker['Building']['full_name'],
        array(
          'controller' => 'buildings',
          'action'     => 'view',
          $placeMarker['Building']['id']
        )
    ); 
    ?>
    </td>
    <td>
    <?php 
    echo $this->Html->link(
        $placeMarker['Room']['number'],
        array(
          'controller' => 'rooms',
          'action'     => 'view',
          $placeMarker['Room']['id']
        )
    ); 
    ?>
    </td>
    <td>
    <?php 
    if(isset($placeMarker['Item']['id'])){
	    echo $this->Html->link(
	        $placeMarker['Item']['serial_num'],
	        array(
	          'controller' => 'item',
	          'action'     => 'view',
	          $placeMarker['Item']['id']
	        )
	    ); 
    }
    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>

<!-- </div> -->
<?php 
echo $this->Html->link(
       'Add an Item', array(
          'controller' => 'item',
          'action'     => 'add')
    );
?>
