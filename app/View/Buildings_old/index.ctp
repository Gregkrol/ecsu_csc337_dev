<!--Buildings--->
<div id = "Buidlings">
	<h2> All Buildings </h2>
	
	
	
	<table>
		<tr>
			<th>Building Name</th>
			<th>QR name</th>
		</tr>
		
		<?php foreach($allBuildings as $building): ?>
		
		<tr>
			<td><?php 
				 echo $this->Html->link($building['Building']['full_name'], array(
				 	'controller' => 'buildings',
				 	'action' => 'view',
				 	 $building['Building']['name']
				 )); ?>
			</td>
			<td><?php echo $building['Building']['name']; ?></td>
		</tr>	
		<?php endforeach; ?>
	</table>
</div>