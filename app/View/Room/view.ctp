<h2>Room <?php echo $Room['Room']['number']; ?> <small><?php echo $Room['Building']['full_name']; ?></small></h2>

<?php print_r($Room);?>
<p>
</p>

<h3>Items</h3>
<table class="table table-striped table-bordered table-condensed">
  <tr>
    <th>Serial Number</th>
  </tr>
  <?php foreach ($Room['Item'] as $item): ?>
  <tr>
    <td>
    <?php 
    echo $this->Html->link(
        $item['serial_num'],
        array(
          'controller' => 'Items',
          'action'     => 'view',
          $item['id']
        )
    ); 
    ?>
    </td>
  </tr>	
  <?php endforeach; ?>
</table>


<p>
	<?php 
	echo $this->Html->link(
		        'Add an item', array(
		        'controller' => 'items',
		        'action'     => 'add',
		        $Room['Room']['id']
		        )
	); 
	?>
</p>