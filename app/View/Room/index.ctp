<!--Rooms--->
<h2> All Rooms <small>ever</small></h2>

<table class="table table-striped table-bordered table-condensed" >
	<tr>
		<th>Building Name</th>
		<th>Room Number</th>
	</tr>
	<?php
	 /*
	  * Here, we are given an object called $allRooms from our Controller. 
	  * We have access to that object. 
	  */
	 foreach($allRooms as $Room): 
	 ?>
	<tr>
		<td><?php // List building, and have it be a link to view building
			echo $this->Html->link(
        		$Room['Building']['full_name'], array(
        		'controller' => 'Buildings',
        		'action'     => 'view',
        		$Room['Building']['id']
        	)); ?>
        </td>
		<td><?php // List room, and have it be a link to view room
			echo $this->Html->link(
        		$Room['Room']['number'], array(
        		'controller' => 'Rooms',
        		'action'     => 'view',
        		$Room['Room']['id']
        	));
			?>
		</td>
	</tr>	
	<?php endforeach; ?>
</table>

<?php 
echo $this->Html->link(
       'Add a Room', array(
          'controller' => 'rooms',
          'action'     => 'add')
    );
?>