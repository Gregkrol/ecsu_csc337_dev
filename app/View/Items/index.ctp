<!--Items--->
<h1> All Items </h1>

<table class="table table-striped table-bordered">
	<thead>
	<tr>
		<th>Type</th>
		<th>Serial Number</th>
		<th>Building</th>
		<th>Room</th>
	</tr>
	</thead>
	<tbody>
	<?php
	 /*
	  * Here, we are given an object called $allItems from our Controller. 
	  * We have access to that object. 
	  */
	  
	foreach($allItems as $Item): ?>
	 
	<tr>
		<td><?php // Item Link
			echo $this->Html->link(
				$Item['Item']['type'], array(
		          'controller' => 'items',
		          'action'     => 'view',
		          $Item['Item']['id'] )
		    ); ?>
		</td>
		<td><?php // Item Link
			echo $this->Html->link(
				$Item['Item']['serial_number'], array(
		          'controller' => 'items',
		          'action'     => 'view',
		          $Item['Item']['id'] )
		    ); ?>
		</td>
		<td><?php // Building Link
			echo $this->Html->link(
				$Item['Building']['name'], array(
		          'controller' => 'buildings',
		          'action'     => 'view',
		          $Item['Building']['id'] )
		    );?>
		</td>
		<td><?php // Room Link
			echo $this->Html->link(
				$Item['Room']['number'], array(
		          'controller' => 'rooms',
		          'action'     => 'view',
		          $Item['Room']['id'] )
		    );?>
	    </td>
	</tr>
	<?php endforeach; ?>
	</tbody>
</table>

<?php 
echo $this->Html->link(
       'Add an Item', array(
          'controller' => 'items',
          'action'     => 'add'),array(
		  'class'      => 'btn')
	
    );
?>
