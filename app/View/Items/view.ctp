<div class = "row-fluid">
	
	<div class = "span12">
		<h2>
		<small>
			<?php // Building Link
				echo $this->Html->link(
				        $Item['Building']['name'], array(
				        'controller' => 'buildings',
				        'action'     => 'view',
				        $Item['Building']['id']
				        )
				); 
			?>
			room : 
		</small>
			<?php // Room Link
				echo $this->Html->link(
				        $Item['Room']['number'], array(
				        'controller' => 'rooms',
				        'action'     => 'view',
				        $Item['Room']['id']
				        )
				);
			?>
		</h2>
	</div>
	
	<?php 
		if($Item['Item']['is_missing'] == 1){
			$hero = "
				<div class=\"span12 hero-unit\">
				  <h1>Warning, Item is misplaced!</h1>
				  <p>This item has been reported as misplaced. Please return it to the proper room.</p>
				  <p>".
				    $this->Html->link(
				        'Item has been returned', array(
				        'controller' => 'items',
				        'action'     => 'returned',
				        $Item['Item']['id']), array(
				        	'class' => 'btn btn-primary btn-large')
					)."
				  </p>
				</div>";
					
			echo $hero;
		}
	?>

	
	<div class = "span12 well">
		<h1><?php echo $Item['Item']['type'].": ".$Item['Item']['serial_number']; ?> </h1>
	</div>
	<div class = "span12">
		<ul class="nav nav-tabs nav-stacked">
		<li>
		<?php 
		    echo $this->Html->link(
		        'Edit',
		        array(
		          'controller' => 'items',
		          'action'     => 'edit',
		          $Item['Item']['id']
		        )); 
		    ?>
		</li>
		<li>
		<?php 
			echo $this->Html->link(
		        '<i class = "icon-qrcode"></i> Print QR code', array(
			        'controller' => 'items',
			        'action'     => 'QR',
			        $Item['Item']['id']			        
			        ), array (
						'escape' => false,
		        	)
	 		);
		?>
		</li>
		</ul>
	</div>
	<div class = "span12 well">
		<h2>Description</h2>
		<p>
			<?php echo $Item['Item']['description']; ?>
		</p>
	</div>
	
	<?php /*
	// Commented out until fixed
	<div class = "span12 well">
		<h2>Time to next Inspection:</h2>
		<p>
			<?php 
			$todaysDate = date("Y-m-d");
			echo (($todaysDate) - ($Item['Item']['next_inspection_date']));
			?>
			Days
		</p>
	</div>
	*/?>
	
	<div class = "span12 well">
		<h2>Information</h2>
		<dl class = "dl-horizontal">
			<dt>Last Inspection</dt>
				<dd><?php echo $Item['Item']['inspect_date']; ?></dd>
			<dt>Year Manufactured</dt>
				<dd><?php echo $Item['Item']['manufactured_year']; ?></dd>
			<dt>Expires</dt>
				<dd><?php echo $Item['Item']['expire_year']; ?></dd>
			<dt>Tag Number</dt>
				<dd><?php echo $Item['Item']['tag_number']; ?></dd>
			<dt>Size</dt>
				<dd><?php echo $Item['Item']['size']; ?></dd>
			<dt>Mounting Type</dt>
				<dd><?php echo $Item['Item']['mounting_type']; ?></dd>
			<dt>Inspector</dt>
				<dd><?php echo $Item['Item']['inspector']; ?></dd>
		</dl>
	</div>
	
	<div class = "span12">
		<h2>Notes</h2>
		<p>
			<?php echo $Item['Item']['notes']; ?>
		</p>
	</div>
</div>
