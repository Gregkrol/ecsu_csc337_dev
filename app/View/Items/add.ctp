<h1>Add an item <small>Room: RB, Building Name Here if exists</small></h1>

<?php
echo $this->Form->create('Item', array(
	'class' => 'well'));

echo $this->Form->input('building_id');
echo $this->Form->input('rooms_id');
echo $this->Form->input('type');
echo $this->Form->input('serial_number');
echo $this->Form->input('expire_year');
echo $this->Form->input('inspect_interval');
echo $this->Form->input('next_inspection_date');
echo $this->Form->input('manufactured_year');
echo $this->Form->input('tag_number');
echo $this->Form->input('size');
echo $this->Form->input('mounting_type');
echo $this->Form->input('inspector');
echo $this->Form->input('notes');
echo $this->Form->input('description');

echo $this->Form->end('Create Item');
?>
