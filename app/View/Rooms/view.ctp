<div class = "well">
<h1>Room <?php echo $Room['Room']['number']; ?> 
	<small><?php 
		echo $this->Html->link(
	        $Room['Building']['name'],
	        array(
	          'controller' => 'Buildings',
	          'action'     => 'view',
	          $Room['Building']['id']
	        )
	    ); ?>
	</small></h1>
</div>

<?php //print_r($Room); // for debugging: remove // to see what we are trying to parse?>

<ul class="nav nav-tabs nav-stacked">
<li>
	<?php 
	    echo $this->Html->link(
	        'Go back to Building',
	        array(
	          'controller' => 'buildings',
	          'action'     => 'view',
	          $Room['Building']['id']
	        )); 
    ?>
</li>
<li>
	<?php 
		echo $this->Html->link(
		        'Add an item', array(
		        'controller' => 'items',
		        'action'     => 'add',
		        $Room['Room']['id']
		        )
		); 
	?>
</li>
<li>
	<?php 
	    echo $this->Html->link(
	        '<i class = "icon-qrcode"></i> Print QR Code',
	        array(
	          'controller' => 'rooms',
	          'action'     => 'QR',
	          $Room['Room']['id']
	        ), array (
				'escape' => false,
        	)); 
    ?>
</li>
</ul>

<h3>Items</h3>
<table class="table table-striped table-bordered ">
<thead>

  <tr>
  	<th>Type</th>
    <th>Serial Number</th>
    <th>Last Inspection Date</th>
    <th>QR code</th>
    <th>Report Missing</th>
  </tr>
  
</thead>
<tbody>
  <?php foreach ($Room['Item'] as $item): ?>
  <tr>
  	<td>
  		<?php echo $item['type'];?>
  	</td>
    <td>
    <?php 
    echo $this->Html->link(
        $item['serial_number'],
        array(
          'controller' => 'Items',
          'action'     => 'view',
          $item['id']
        )
    ); 
    ?>
    </td>
    <td>
    	<?php echo $item['inspect_date'];?>
    </td>
    <td>
    	<?php 
		echo $this->Html->link(
			        '<i class = "icon-qrcode"></i>', array(
			        'controller' => 'items',
			        'action'     => 'QR',
			        $item['id']			        
			        ), array (
						'escape' => false,
			        	)
			 );
		?>
    </td>
    <td>
    	<?php 
		echo $this->Html->link(
			        'Missing', array(
			        'controller' => 'items',
			        'action'     => 'missing',
			        $item['id']
			        ),array(
			 		 'class'      => 'btn btn-danger')
		); 
		?>
    </td>
  </tr>	
  <?php endforeach; ?>
</tbody>
</table>

<p>
	
</p>