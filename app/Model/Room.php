<?php
/*
 * Our model for a room
 *
 */
 
class Room extends AppModel {
        //Declare variables as public
       
        public $name = 'Room'; //not needed in cakePHP 2.0+
        /**/
        public $belongsTo = 'Building';
        public $hasMany = array(
                'Item' => array(
                        'className'  => 'items',
                        'foreignKey' => 'rooms_id')
                );
       
    var $validates = array(
        'building_id' => array(
            'rule' => 'numeric',
            'required' => true
        ),
        'number' => array(
            'rule' => 'alphaNumeric',
            'required' => true
        )
    );
}