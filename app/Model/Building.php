<?php
/*
 * Our model for Building
 *
 */
 
class Building extends AppModel {
       
        public $name = 'Building';
        public $hasMany = array('Room');
       
        var $validate = array(
        'name' => array(
            'rule' => 'alphaNumeric',
            'required' => true
        )
    );
}
?>