<?php
/*
 * Our model for a Item
 *
 */
 
class Item extends AppModel {
        //Declare variables as public
       
        public $name = 'Item'; //not needed in cakePHP 2.0+
       
        public $belongsTo = array(
                'Room' => array(
                        'className'  => 'Room',
                        'foreignKey' => 'rooms_id'),
                'Building' => array(
                        'classname'  => 'Building',
                        'foreignKey' => 'building_id')
        );
 
    var $validate = array(
        'rooms_id' => array (
            'rule' => 'numeric',
            'required' => true
        ),
        'building_id' => array (
            'rule' => 'numeric',
            'required' => true
        ),
        'manufactured_year' => array (
            // No Year only validation, 0-4999 should work
            'rule' => array('range', -1, 5000)
        ),
        'expired_year' => array (
            // No Year only validation, 0-4999 should work
            'rule' => array('range', -1, 5000)
        ),
        //inspect_date is not validated, no dateTime validation
        'next_inspection_date' => array(
            // Date in Month(name) day, year format (for example: May 1, 1999)
            'rule' => array('date', 'Mdy')
        ),
        'tag_number' => array(
            'rule' => 'numeric'
        ),
        'serial_number' => array(
            'rule' => 'alphaNumeric',
            'required' => true
        ),
        'mounting_type' => array(
            'rule' => 'alphaNumeric'
        ),
        'type' => array(
            'rule' => 'alphaNumeric'
        ),
        'inspector' => array(
            'rule' => 'alphaNumeric'
        ),
        'notes' => array(
            'rule' => 'alphaNumeric'
        ),
        'description' => array(
            'rule' => 'alphaNumeric'
        ),
        'inspect_interval' => array(
            'rule' => 'numeric'
        ),
        'is_missing' => array (
            'rule' => 'boolean'
        )
    ); 
}