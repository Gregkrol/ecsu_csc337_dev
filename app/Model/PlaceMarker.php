<?php
/*
 * Our model for a Place Marker
 * 
 */

class PlaceMarker extends AppModel {
	//Declare variables as public
	
	public $name = 'PlaceMarker'; //not needed in cakePHP 2.0+
	
	public $belongsTo = array(
		'Room',
		'Building' => array(
				'className'  => 'Building',
				'foreignKey' => 'room_building_id')
	);
	
	public $hasMany = array(
		'Item' => array(
			'classNmae'  => 'Item',
			'foreignKey' => 'place_markers_id'));
	
}