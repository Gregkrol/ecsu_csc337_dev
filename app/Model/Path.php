<?php
/*
 * Our model for a Path
 * 
 */

class Path extends AppModel {
	//Declare variables as public
	
	public $name = 'Path'; //not needed in cakePHP 2.0+
	
	/**/
	public $belongsTo = array(
		'Building' => array(
			'className'  => 'Building',
			'foreignKey' => 'buildings_id'),
		'Room' => array(
			'className'  => 'Room',
			'foreignKey' => 'rooms_id'),
		'PlaceMarker' => array(
			'className'  => 'PlaceMarker',
			'foreignKey' => 'place_markers_id'),
		'Item' => array(
			'className'  => 'Item',
			'foreignKey' => 'items_id'));
	
	public $validate = array(
		'name' => array(
			'alphaNumeric' => array(
				'rule'     => 'alphaNumeric',
				'required' => true,
				'message'  => 'No Path Specified'),
			'between' => array(
				'rule'    => array('between', 1, 40),
				'message' => 'Between 1 and 40 characters, please')
		));
}