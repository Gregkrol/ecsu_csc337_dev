<?php
/*
 * Our model for a room
 * 
 */

class LocationId extends AppModel {
	//Declare variables as public
	
	public $name = 'LocationId'; //not needed in cakePHP 2.0+
	public $hasMany = 'Item';
	
	public $belongsTo = array(
		'Room' => array(
			'className' => 'rooms',
			'foreignKey' => 'room_id'
			)
	);
	
}