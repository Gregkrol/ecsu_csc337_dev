# This is my README

CakePHP application writ for Eastern Connecticut State University's Public Safety department to keep track of Fire extinguishers on campus. 

We are streamlining their process by simply adding codes to each thing. 

# CakePHP readme
	Finding data: http://book.cakephp.org/2.0/en/models/retrieving-your-data.html
	Forms stuff: http://book.cakephp.org/2.0/en/core-libraries/helpers/form.html (see ItemsController.php - add)
	
	
=======================================
What they use MS Access for:

"
a)keep track of the locations of approximately 1200 extinguishers on campus

b)keep track of serial numbers and dates due for 6 year and 12 year inspections.

i.This is determined by taking the year stamped on the extinguisher (or sometimes the last contractor 6 year inspection date instead) and adding 6 years to it).

c)       to provide an order to routes in each building for students doing the monthly inspections

i.         this means separating out the monthly inspections from the yearly inspections.

ii.   some of the housing has extinguishers in each student�s room, we don�t inspect these on a monthly basis.  This is done once a year with a contractor.

d)      we currently include monthly oil tank inspections in 13 locations, emergency kit inspections in 6 locations, and crane inspections in 2 locations.

e)      to determine cost of outside contractor coming in each year.  We do this with a report that includes a count of

i.                     2.5#, 5#, 10#, 20#, K2, extinguishers needing 6 year inspections

ii.                   2.5#, 5#, 10#, 20#, K2, extinguishers needing 12 year inspections

iii.                  Current count of all extinguishers on campus with subtotals by weight and type (BC, ABC, K2 CO2).  This includes ones we have in a storage cage in Mead
and also the extinguishers in vehicles on campus, part of the Facilities garage, both done once a year.

iv.                 Includes report of count of weight and type for each building.


2.       Biggest use is for the monthly inspections of approximately 600 extinguishers and second is the report to determine cost of outside contractor inspections.


3.       The crane, emergency kit and oil tank inspections are currently done on ipad html survey since there are a series of questions for each of these.
And yes, this can be incorporated into the database and is also included now.



4.       The next demonstration could be at Eastern Hall.  We have 4 extinguishers at Eastern Hall.
And also at Laurel Hall.  Laurel Hall has a total of 112 extinguishers.  41 of these are done each month, we have

                 a route for you to follow on each of these."
                 