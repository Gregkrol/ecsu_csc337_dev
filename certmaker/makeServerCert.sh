#!/bin/bash
#  This batch file will create a server certificate
#
#  Set the openssl config file 
#
#  set OPENSSL_CONF=./bin/openssl.cnf

#
#  create the folders and files needed by openssl
#  Delete "old" files if they exist
#

if [ -z $1 ]
  then
    echo "Sorry, you must enter a name."
    echo "syntax: bash make$NAME.sh [name]"
    exit
fi

NAME=$1

CADIR="/etc/ssl/CA"

if [ ! -e "$CADIR/server" ]; then
 mkdir $CADIR/server
fi
if [ ! -e "$CADIR/server/certificates" ]; then
 mkdir $CADIR/server/certificates
fi
if [ ! -e "$CADIR/server/requests" ]; then
 mkdir $CADIR/server/requests
fi
if [ ! -e "$CADIR/server/keys" ]; then
 mkdir $CADIR/server/keys
fi

#
# generate webserver key
#

openssl genrsa -des3 -out  $CADIR/server/keys/$NAME.key 1024


#
# Create the certificate signing request (csr)
#


openssl req -new -key $CADIR/server/keys/$NAME.key -out $CADIR/server/requests/$NAME.csr

#
#  Remove the passphrase from the key
#
#  copy $NAME.key $NAME.key.org
#  bin\openssl rsa -in $NAME.key.org -out $NAME.key

#
#  sign the server csr with the CA key
#

openssl ca -in $CADIR/server/requests/$NAME.csr -cert $CADIR/CA.crt -keyfile $CADIR/CA.key -out $CADIR/server/certificates/$NAME.crt

#
# display the signed server certificate
#
openssl x509 -in $CADIR/server/certificates/$NAME.crt -text

#
# remove pass phrase
#

openssl rsa -in $CADIR/server/keys/$NAME.key -out $CADIR/server/keys/$NAME.key 

# The End
