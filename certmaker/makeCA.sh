#!/bin/bash
#  This batch file will create a CA store
#
#  Set the openssl config file 
#
cd /etc/ssl

#
#  create the CA folder if it does not exist
#
if [ ! -e "/etc/ssl/CA" ]; then
 mkdir /etc/ssl/CA
fi

# make serial and index file
if [ ! -e "/etc/ssl/CA/serial" ]
  then
    echo 1000 > /etc/ssl/CA/serial
    chmod 600 /etc/ssl/CA/serial
fi

if [ ! -e "/etc/ssl/CA/index.txt" ]
  then
    touch /etc/ssl/CA/index.txt
    chmod 600 /etc/ssl/CA/index.txt
fi


#
#  Generate the CA key
#
openssl genrsa -out /etc/ssl/CA/CA.key

#
#  Create the certificate signing request (csr)
#
openssl req -new -key /etc/ssl/CA/CA.key -out /etc/ssl/CA/CA.csr

#
#  Self sign the CA csr
#
openssl x509 -req -days 365 -in /etc/ssl/CA/CA.csr -out /etc/ssl/CA/CA.crt -signkey /etc/ssl/CA/CA.key

#
# display the signed CA certificate
#
openssl x509 -in /etc/ssl/CA/CA.crt -text


#
# The End
 
