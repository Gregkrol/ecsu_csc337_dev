#!/bin/bash
#  This batch file will create a client certificate
#
#  Set the openssl config file 
#
#  set OPENSSL_CONF=./bin/openssl.cnf

#
# make target directories
#

if [ -z $1 ]
  then
    echo "Sorry, you must enter a name."
    echo "syntax: bash makeClientCert.sh [name]"
    exit
fi

NAME=$1

CADIR="/etc/ssl/CA"

if [ ! -e "$CADIR/user" ]; then
 mkdir $CADIR/user
fi
if [ ! -e "$CADIR/user/certificates" ]; then
 mkdir $CADIR/user/certificates
fi
if [ ! -e "$CADIR/user/requests" ]; then
 mkdir $CADIR/user/requests
fi
if [ ! -e "$CADIR/user/keys" ]; then
 mkdir $CADIR/user/keys
fi
if [ ! -e "$CADIR/user/p12" ]; then
 mkdir $CADIR/user/p12
fi

#
#  Generate the client key
#

openssl genrsa -des3 -out  $CADIR/user/keys/$NAME.key 1024

#
#  Create the certificate signing request (csr)
#
openssl req -new -key $CADIR/user/keys/$NAME.key -out $CADIR/user/requests/$NAME.csr

#
#  sign the client csr with the CA key
#
openssl ca -in $CADIR/user/requests/$NAME.csr -cert $CADIR/CA.crt -keyfile $CADIR/CA.key -out $CADIR/user/certificates/$NAME.crt

#
# display the signed client certificate
#
openssl x509 -in $CADIR/user/certificates/$NAME.crt -text

#
# Create the p12 file to import into clients
#
openssl pkcs12 -export -clcerts -in $CADIR/user/certificates/$NAME.crt -inkey $CADIR/user/keys/$NAME.key -out $CADIR/user/p12/$NAME.P12

#  The End
