SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS k11943_Dev ;
CREATE SCHEMA IF NOT EXISTS k11943_Dev DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE k11943_Dev ;

-- -----------------------------------------------------
-- Table k11943_Dev.buildings
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.buildings ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.buildings (
  id INT NOT NULL AUTO_INCREMENT ,
  name VARCHAR(100) NOT NULL ,
  PRIMARY KEY (id) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.rooms
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.rooms ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.rooms (
  id INT NOT NULL AUTO_INCREMENT ,
  building_id INT NOT NULL ,
  number VARCHAR(45) NULL ,
  PRIMARY KEY (id, building_id) ,
  INDEX fk_rooms_buildings (building_id ASC) ,
  CONSTRAINT fk_rooms_buildings
    FOREIGN KEY (building_id )
    REFERENCES k11943_Dev.buildings (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.items
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.items ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.items (
  id INT NOT NULL AUTO_INCREMENT ,
  rooms_id INT NOT NULL ,
  building_id INT NOT NULL ,
  manufactured_year YEAR NULL ,
  expire_year YEAR NULL ,
  inspect_date DATETIME NULL ,
  next_inspection_date DATE NULL ,
  tag_number VARCHAR(45) NULL ,
  size VARCHAR(45) NULL ,
  serial_number VARCHAR(45) NULL ,
  mounting_type VARCHAR(45) NULL ,
  type VARCHAR(45) NULL ,
  inspector VARCHAR(45) NULL ,
  notes VARCHAR(500) NULL ,
  description VARCHAR(200) NULL ,
  PRIMARY KEY (id, rooms_id, building_id) ,
  INDEX fk_items_rooms1 (rooms_id ASC, building_id ASC) ,
  CONSTRAINT fk_items_rooms1
    FOREIGN KEY (rooms_id , building_id )
    REFERENCES k11943_Dev.rooms (id , building_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
