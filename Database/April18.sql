SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS k11943_Dev ;
CREATE SCHEMA IF NOT EXISTS k11943_Dev DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE k11943_Dev ;

-- -----------------------------------------------------
-- Table k11943_Dev.`buildings`
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.`buildings` ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.`buildings` (
  `id` INT NOT NULL ,
  `qr_name` VARCHAR(45) NULL ,
  `full_name` VARCHAR(45) NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.`rooms`
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.`rooms` ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.`rooms` (
  `id` INT NOT NULL ,
  `number` VARCHAR(45) NULL ,
  `building_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `building_id`) ,
  INDEX `fk_rooms_buildings` (`building_id` ASC) ,
  CONSTRAINT `fk_rooms_buildings`
    FOREIGN KEY (`building_id` )
    REFERENCES k11943_Dev.`buildings` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.`items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.`items` ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.`items` (
  `id` INT NOT NULL ,
  `serial_num` VARCHAR(45) NULL ,
  `room_id` INT NOT NULL ,
  `room_building_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `room_id`, `room_building_id`) ,
  INDEX `fk_items_rooms1` (`room_id` ASC, `room_building_id` ASC) ,
  CONSTRAINT `fk_items_rooms1`
    FOREIGN KEY (`room_id` , `room_building_id` )
    REFERENCES k11943_Dev.`rooms` (`id` , `building_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.`location_ids`
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.`location_ids` ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.`location_ids` (
  `id` INT NOT NULL ,
  `room_id` INT NOT NULL ,
  `room_building_id` INT NOT NULL ,
  `item_id` INT NOT NULL ,
  `item_room_id` INT NOT NULL ,
  `item_room_building_id` INT NOT NULL ,
  PRIMARY KEY (`id`, `room_id`, `room_building_id`) ,
  INDEX `fk_location_ids_rooms1` (`room_id` ASC, `room_building_id` ASC) ,
  INDEX `fk_location_ids_items1` (`item_id` ASC, `item_room_id` ASC, `item_room_building_id` ASC) ,
  CONSTRAINT `fk_location_ids_rooms1`
    FOREIGN KEY (`room_id` , `room_building_id` )
    REFERENCES k11943_Dev.`rooms` (`id` , `building_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_location_ids_items1`
    FOREIGN KEY (`item_id` , `item_room_id` , `item_room_building_id` )
    REFERENCES k11943_Dev.`items` (`id` , `room_id` , `room_building_id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO `k11943_Dev`.`buildings` (`id`, `qr_name`, `full_name`) VALUES ('1', 'SCNCE', 'Science Building');
INSERT INTO `k11943_Dev`.`items` (`id`, `serial_num`, `room_id`, `room_building_id`) VALUES (NULL, '11111', '1', '1');
