SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS k11943_Dev ;
CREATE SCHEMA IF NOT EXISTS k11943_Dev DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci ;
USE k11943_Dev ;

-- -----------------------------------------------------
-- Table k11943_Dev.buildings
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.buildings ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.buildings (
  id INT NOT NULL ,
  full_name VARCHAR(45) NOT NULL ,
  qr_name VARCHAR(45) NULL ,
  PRIMARY KEY (id) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.rooms
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.rooms ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.rooms (
  id INT NOT NULL ,
  number VARCHAR(45) NOT NULL ,
  building_id INT NOT NULL ,
  PRIMARY KEY (id, building_id) ,
  INDEX fk_rooms_buildings (building_id ASC) ,
  CONSTRAINT fk_rooms_buildings
    FOREIGN KEY (building_id )
    REFERENCES k11943_Dev.buildings (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.place_markers
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.place_markers ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.place_markers (
  id INT NOT NULL ,
  room_id INT NOT NULL ,
  room_building_id INT NOT NULL ,
  PRIMARY KEY (id, room_id, room_building_id) ,
  INDEX fk_location_ids_rooms1 (room_id ASC, room_building_id ASC) ,
  CONSTRAINT fk_location_ids_rooms1
    FOREIGN KEY (room_id , room_building_id )
    REFERENCES k11943_Dev.rooms (id , building_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.items
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.items ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.items (
  id INT NOT NULL ,
  serial_num VARCHAR(45) NULL ,
  place_markers_id INT NOT NULL ,
  place_markers_room_id INT NOT NULL ,
  place_markers_room_building_id INT NOT NULL ,
  PRIMARY KEY (id, place_markers_id, place_markers_room_id, place_markers_room_building_id) ,
  INDEX fk_items_place_markers1 (place_markers_id ASC, place_markers_room_id ASC, place_markers_room_building_id ASC) ,
  CONSTRAINT fk_items_place_markers1
    FOREIGN KEY (place_markers_id , place_markers_room_id , place_markers_room_building_id )
    REFERENCES k11943_Dev.place_markers (id , room_id , room_building_id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table k11943_Dev.paths
-- -----------------------------------------------------
DROP TABLE IF EXISTS k11943_Dev.paths ;

CREATE  TABLE IF NOT EXISTS k11943_Dev.paths (
  id INT NOT NULL ,
  name VARCHAR(45) NOT NULL ,
  last_visit DATETIME NULL ,
  buildings_id INT NOT NULL ,
  rooms_id INT NOT NULL ,
  place_markers_id INT NOT NULL ,
  items_id INT NOT NULL ,
  PRIMARY KEY (id, name) ,
  INDEX fk_paths_items1 (items_id ASC) ,
  INDEX fk_paths_place_markers1 (place_markers_id ASC) ,
  INDEX fk_paths_rooms1 (rooms_id ASC) ,
  INDEX fk_paths_buildings1 (buildings_id ASC) ,
  CONSTRAINT fk_paths_items1
    FOREIGN KEY (items_id )
    REFERENCES k11943_Dev.items (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_paths_place_markers1
    FOREIGN KEY (place_markers_id )
    REFERENCES k11943_Dev.place_markers (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_paths_rooms1
    FOREIGN KEY (rooms_id )
    REFERENCES k11943_Dev.rooms (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT fk_paths_buildings1
    FOREIGN KEY (buildings_id )
    REFERENCES k11943_Dev.buildings (id )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
